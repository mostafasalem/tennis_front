import { Injectable }    from '@angular/core';   
import { HttpClient, HttpHeaders } from '@angular/common/http' 
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Matche } from '../models/matche'; 
import { Participant } from '../models/participant';  
import { Response } from '../models/response'; 



@Injectable()  
export class MatcheService {  
  constructor(private http: HttpClient) { }  
  private matcheUrl = 'http://localhost:8080/api/matche/firstRoundMatches'  
  private playersUrl = 'http://localhost:8080/api/participant/getAllParticipants'

  getAllMatches(): Observable<Response> {  
    return this.http.get<Response>(this.matcheUrl);
  }  

  getPlayers(): Observable<Response> {  
    return this.http.get<Response>(this.playersUrl);
  }  

}  
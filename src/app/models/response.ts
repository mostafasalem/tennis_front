
export class Response {
    status: string;
    timestamp: string;
    data: any[];
}


export class Participant {
    id: number;
    name: string;
    // 0 : female
    // 1 : male
    gender: number;
    leagueId: number;
}
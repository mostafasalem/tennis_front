import { Participant } from './participant'; 

export class MatcheView {
    id: number;
    date: Date;
    player1: Participant;
    player2: Participant;
    winner: Participant;
    result: string;
}
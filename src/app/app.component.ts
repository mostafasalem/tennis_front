import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Matche } from './models/matche'; 
import { Participant } from './models/participant'; 
import { MatcheView } from './models/matcheView'; 
import { MatcheService } from './services/matche.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../assets/css/bootstrap.min.css', '../assets/css/font-awesome.min.css']
})

export class AppComponent implements OnInit {
  title = 'tennis';
  private matches: Matche[] = []; 
  public players: Participant[] = [];
  public matcheView: MatcheView[] = [];
  public viewTable = '';

  constructor(private matcheService: MatcheService) {  } 


  ngOnInit() {  
    this.players = [];
    this.matches = [];
    this.matcheView = [];
    this.viewTable = '';
  }  

  getAllPlayers() {
    this.viewTable = 'players';
    this.matcheService.getPlayers().subscribe((res) => { 
      this.players = res.data
    }); 
  }

  getAllMatches() {
    if(this.players.length == 0) {
      this.getAllPlayers();
    }
    this.viewTable = 'matches';
    this.matcheService.getAllMatches().subscribe((res) => { 
    	this.matches = res.data;
      this.updatePlayersName();
    }); 
  }

  updatePlayersName() {
    if(this.players.length > 0 && this.matches.length > 0) {
      this.matches.forEach(m => {
        let mv = new MatcheView();
        mv.id = m.id;
        mv.player1 = this.players.find(p => p.id == m.player1);
        mv.player2 = this.players.find(p => p.id == m.player2);
        mv.winner = this.players.find(p => p.id == m.winner);
        this.matcheView.push(mv);
      });
    }
  }







}
